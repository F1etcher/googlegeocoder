<?php

namespace App\Http\Controllers;


use App\Google\GoogleGeocoder;

class SiteController extends Controller
{

    /**
     * Return geocoding value
     *
     * @param GoogleGeocoder $geocoder
     * @return array|string
     */
    public function geocoding(GoogleGeocoder $geocoder)
    {
        return $geocoder->geocoder('277 Bedford Avenue, Brooklyn, NY 11211, USA', '', 'ru', true);
    }

    /**
     * Return reverse geocoding value
     *
     * @param GoogleGeocoder $geocoder
     * @return array|string
     */
    public function reverseGeocoding(GoogleGeocoder $geocoder)
    {
        return $geocoder->geocoder('', '34.172684,-118.604794', 'en', false);
    }

}
