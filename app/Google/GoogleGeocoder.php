<?php

namespace App\Google;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Support\Facades\Cache;

class GoogleGeocoder
{

    /**
     * Google Application key
     *
     * @var
     */
    private $applicationKey;

    /**
     * Request uri
     *
     * @var
     */
    private $requestUri;

    /**
     * Disable caching
     *
     * @var bool
     */
    private $cached = false;


    /**
     * Setup app key and uri
     *
     * GoogleGeocoder constructor.
     * @param $config
     */
    public function __construct($config)
    {
        $this->applicationKey = $config['applicationKey'];
        $this->requestUri = $config['requestUri'];
    }

    /**
     * Receive and validate parameters
     *
     * @param null $address
     * @param null $latlng
     * @param null $lang
     * @param null $cached
     * @return array|string
     */
    public function geocoder($address = null, $latlng = null, $lang = null, $cached = null)
    {
        if (empty($address) && empty($latlng)) return 'Pass address or latlng';

        $query = [];
        if (!empty($address)) $query['address'] = $address;
        if (!empty($latlng)) $query['latlng'] = $latlng;
        if (!empty($lang)) $query['language'] = $lang;
        if (!empty($cached) && is_bool($cached)) $this->cached = $cached;

        return $this->sendRequest($query);
    }

    /**
     * Sending request to google maps api
     *
     * @param $query
     * @return array|string
     */
    private function sendRequest($query)
    {
        $client = new Client();

        try {
            $request = $client->get($this->requestUri . $this->applicationKey, [
                'query' => $query
            ]);
        } catch (RequestException $e) {
            if ($e->hasResponse()) return Psr7\str($e->getResponse());
            return Psr7\str($e->getRequest());
        }

        $result = $request->getBody()->getContents();

        return $this->parseRequest(json_decode($result)->results);
    }

    /**
     * Parse results
     *
     * @param $data
     * @return array
     */
    private function parseRequest($data)
    {
        $result = [];

        foreach ($data as $key => $address) {
            $result[$key] = [
                'formatted_address' => $address->formatted_address,
                'location' => $address->geometry->location,
            ];
        }

        if ($this->cached) Cache::put('address', $result, config('services.google.geocoder.cacheTime'));

        return $result;
    }

}