<?php

namespace App\Providers;

use App\Google\GoogleGeocoder;
use Illuminate\Support\ServiceProvider;

class GoogleGeocoderServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(GoogleGeocoder::class, function () {
            return new GoogleGeocoder(config('services.google.geocoder'));
        });
    }
}
